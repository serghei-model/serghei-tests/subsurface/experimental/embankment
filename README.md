## Subsurface Test: 2D Embankment

This is the road embankment problem reported in https://doi.org/10.1016/j.compgeo.2018.05.003. It models infiltration into an inclined road embankment with a fixed pressure at the bottom.
